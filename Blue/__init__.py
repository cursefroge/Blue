import socket
import sys
import logging
import re


class BlueConfig:
    def __init__(self, port=8080, routes=None, middlewares=None, loglevel=logging.INFO):
        if middlewares is None:
            middlewares = []
        if routes is None:
            routes = {}
        self.port = port
        self.routes = routes
        self.middlewares = middlewares
        self.loglevel = loglevel

    def use(self, middleware):
        self.middlewares.append(middleware)


def fromfile(file_path):
    def handler(req):
        try:
            with open(file_path) as f:
                content = f.read()
            return content, 200
        except FileNotFoundError:
            return "404 Not Found", 404
        except PermissionError:
            return "403 Forbidden", 403

    return handler


def serve(config: BlueConfig):
    # Create a logger
    stdout_logger = logging.getLogger('stdout_logger')
    stdout_logger.setLevel(config.loglevel)  # Set the logger level to WARNING
    stdout_logger.propagate = False  # Prevent logs from being passed to the parent logger

    # Create a handler for stdout
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(config.loglevel)  # Set the handler level to WARNING

    # Create a logger for stderr
    stderr_logger = logging.getLogger('stderr_logger')
    stderr_logger.setLevel(config.loglevel)  # Set the logger level to ERROR
    stderr_logger.propagate = False  # Prevent logs from being passed to the parent logger

    # Create a handler for stderr
    stderr_handler = logging.StreamHandler(sys.stderr)
    stderr_handler.setLevel(config.loglevel)  # Set the handler level to ERROR

    # Create a formatter and attach it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stdout_handler.setFormatter(formatter)
    stderr_handler.setFormatter(formatter)

    # Add the handlers to the loggers
    stdout_logger.addHandler(stdout_handler)
    stderr_logger.addHandler(stderr_handler)

    def info(msg, *args, **kwargs):
        stdout_logger.info(msg, *args, **kwargs)

    def debug(msg, *args, **kwargs):
        stdout_logger.debug(msg, *args, **kwargs)

    def error(msg, *args, **kwargs):
        stderr_logger.error(msg, *args, **kwargs)

    def warn(msg, *args, **kwargs):
        stderr_logger.warning(msg, *args, **kwargs)

    # HTTP server implemented from scratch with sockets
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('localhost', config.port))
    server_socket.listen(1)
    info('Server is running on http://localhost:{}'.format(config.port))
    while True:
        client_socket, client_address = server_socket.accept()
        request = client_socket.recv(1024).decode()
        route = request.split(' ')[1]
        debug(f"Serving {client_address[0]} {route}")
        # get the route handler
        handler = None
        for pattern, potential_handler in config.routes.items():
            if re.fullmatch(pattern, route):
                handler = potential_handler
                break
        if handler is None:
            error("giving user 404")
            client_socket.sendall("HTTP/1.1 404 Not Found\n\n404 Not Found".encode())
            client_socket.close()
            continue
        else:
            # call the handler
            result = handler(request)
            if isinstance(result, tuple):
                resp, status = result
            else:
                resp = result
                status = 200
            resp = f"HTTP/1.1 {status} OK\n\n" + resp

        client_socket.sendall(resp.encode())
        client_socket.close()
