import logging

from Blue import BlueConfig, fromfile, serve

cfg = BlueConfig(
    port=8080,
    routes={
        "/": fromfile("index.html"),
        "/fard": lambda req: ("418 I'm a teapot", 418),
        r"/user/\d+": lambda req: ("User page", 200),
    },
    middlewares=[],
    loglevel=logging.DEBUG
)
serve(cfg)
